import {Component, OnDestroy, OnInit} from '@angular/core';
import {RegisterComponent} from "../register/register.component";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  subs: Subscription[] = [];

  constructor(private authService: AuthService,
              private afAuth: AngularFireAuth,
              private router: Router,
              private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.subs.push(this.authService.UserData.subscribe(data => {
      if (data) {
        this.router.navigateByUrl('/').then();
      }
    }));

  }
  openRegister(): void {
    const dialogRef = this.matDialog.open(RegisterComponent, {
      role: 'dialog',
      height: '480px',
      width: '480px'
    });

    dialogRef.afterClosed().subscribe(result => {
      const {fname, lname, email, password, avatar} = result;

      if (result !== undefined) {
        this.authService.signUp(email, password, fname, lname, avatar);
      }

      return;
    });
  }

  login(form: NgForm): void {
    const {email, password} = form.value;
    if (!form.valid) {
      return;
    }
    this.authService.signIn(email, password);
    form.resetForm();
  }

  ngOnDestroy(): void {
    this.subs.map(s => s.unsubscribe());
  }
}
