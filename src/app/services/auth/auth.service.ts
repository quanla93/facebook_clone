import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {BehaviorSubject, Observable} from "rxjs";
import firebase from "firebase/compat/app";
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userData: Observable<firebase.User>;

  private currentUser: UserData;

  private currentUser$ = new BehaviorSubject<UserData>(null);

  constructor(private afs: AngularFirestore,
              private afsAuth: AngularFireAuth,
              private router: Router) {

    this._userData = afsAuth.authState;

    this._userData.subscribe(user => {
      if(user){
        this.afs.collection<UserData>('users')
          .doc<UserData>(user.uid)
          .valueChanges()
          .subscribe(currentUser => {
            if (currentUser != undefined) {
              this.currentUser = currentUser;
              this.currentUser$.next(this.currentUser);
            } else {
              this.currentUser = null;
              this.currentUser$.next(this.currentUser);
            }
          });
      }
    });
  }

  CurrentUser(): Observable<UserData> {
    return this.currentUser$.asObservable();
  }

  signUp(email: string,
         password: string,
         firstName: string,
         lastName: string,
         avatar = 'https://portal.staralliance.com/cms/aux-pictures/prototype-images/avatar-default.png/@@images/image.png'): void {
    this.afsAuth.createUserWithEmailAndPassword(email, password)
      .then(res => {
        if(res) {
          this.afs.collection('users').doc(res.user.uid)
            .set({
              firstName,
              lastName,
              email,
              avatar
            }).then(() => {
              this.afs.collection<UserData>('users')
                .doc<UserData>(res.user.uid)
                .valueChanges()
                .subscribe(user => {
                  if (user) {
                    this.currentUser = user;
                    this.currentUser$.next(this.currentUser);
                  }
                })
          })
        }
      }).catch(err => console.log(err));
  }

  get UserData(): Observable<firebase.User> {
    return this._userData;
  }

  signIn(email: string, password: string): void {
    this.afsAuth.signInWithEmailAndPassword(email, password)
      .then(res => {
        this._userData = this. afsAuth.authState;
            this.afs.collection<UserData>('users')
              .doc<UserData>(res.user.uid)
              .valueChanges()
              .subscribe(user => {
                if (user) {
                  this.currentUser = user;
                  this.currentUser$.next(this.currentUser);
                  // this.toastr.success('Login Successful');
                }
              });
      }).catch(err => console.log(err));
  }

  Logout(): void {
    this.afsAuth.signOut().then(res => {
      console.log(res);
      this.currentUser = null;
      this.currentUser$.next(this.currentUser);
      this.router.navigateByUrl('/login').then();
    });
  }

  searchUserInDatabase(user_id: string): Observable<UserData> {
    return this.afs.collection<UserData>('users').doc<UserData>(user_id).valueChanges();
  }
}

export interface UserData {
  firstName: string;
  lastName: string;
  avatar: string;
  email: string;
  id?: string;
};
